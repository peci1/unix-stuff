" Steve Peters .vimrc file
" see http://nvie.com/posts/how-i-boosted-my-vim/ for more tips
set nocompatible
set backspace=2
syntax on
set hidden
colorscheme ir_black
set number
set nowrap
set ruler
set expandtab
set tabstop=2
set shiftwidth=2
set autoindent
set showmatch
set hlsearch
set incsearch
set nobackup
set noswapfile
" Filename completion like bash
set wildmode=longest:list
" Use xml highlighting for sdf, world files
autocmd BufEnter *.sdf set filetype=xml
autocmd BufEnter *.world set filetype=xml
autocmd BufEnter *.launch set filetype=xml
